function [xEst, PEst] = uif(x, P, f, u, Q, h, z, R)
% Unscented Kalman Filter
% Input:
%       x: previous estimated state
%       P: previous estimated covariance
%       f: transition function
%       u: system input
%       Q: system error covariance
%       h: sensor function
%       z: measurement
%       R: sensor error covariance
% Output:
%       xEst: estimated state
%       PEst: estimated covariance

n = numel(x);
m = numel(z);
alpha = 1;
beta = 1;
kappa = 3-n;

[X, Wm, Wc, L] = genSigmas(x, P, alpha, beta, kappa);

%%% Prediction Step

f = str2func(f);
Xpred = zeros(n, L);

for i=1:L
    Xpred(:,i) = f(X(:,i), u);
end

MuXpred = sum(Xpred*diag(Wm),2); % predicted state
diffXpred = Xpred - repmat(MuXpred, 1, L);
PXpred = diffXpred*diag(Wc)*diffXpred' + Q; % predicted covariance

Y = eye(size(PXpred))/PXpred; % information matrix
y = Y*MuXpred; % information vector

%%% Update Step

if ~isempty(z)
    h = str2func(h);
    Zpred = zeros(m, L);
    for i=1:L
        Zpred(:,i) = h(Xpred(:,i));
    end

    MuZpred = sum(Zpred*diag(Wm));
    diffZpred = Zpred - repmat(MuZpred, 1, L);
    PZpred = diffZpred*diag(Wc)*diffZpred' + R; % innovation cov + R (S)
    PXZpred = diffXpred*diag(Wc)*diffZpred'; % cross-cov (T)

    iM = (Y*PXZpred/R)*PXZpred'*Y'; % information measurment matrix
    iV = (Y*PXZpred/R)*(z-h(MuXpred)+PXZpred'*y); % information measurement vector
    
    yEst = y + iV;
    YEst = Y + iM;
    PEst = eye(size(YEst))/YEst;
    xEst = PEst*yEst;
    
else
    xEst = MuXpred;
    PEst = PXpred;
end
end

