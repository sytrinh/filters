function [xEst, PEst] = enkf(x, P, f, u, Q, h, z, R)
%COV Summary of this function goes here
%   Detailed explanation goes here

n = numel(x);
m = numel(z);
L = 100;
X = genPoints(x, P, L);
X = X';
%%%%%%%
f = str2func(f);
Xpred = zeros(n, L);
for i=1:L
    Xpred(:,i) = f(X(:,i), u);
end
  
MuXpred = mean(Xpred,2);
diffXpred = Xpred - repmat(MuXpred, 1, L);
PXpred = diffXpred*diffXpred'/(L-1) + Q;

%%%%%
if ~isempty(z)
    h = str2func(h);
    Zpred = zeros(m, L);
    for i=1:L
        Zpred(:,i) = h(Xpred(:,i));
    end

    MuZpred = mean(Zpred);
    diffZpred = Zpred - repmat(MuZpred, 1, L);
    PZpred = diffZpred*diffZpred'/(L-1) + R;
    PXZpred = diffXpred*diffZpred'/(L-1);

    K = PXZpred/PZpred;
    xEst = MuXpred + K*(z-MuZpred);
    PEst = PXpred - K*PZpred*K';
else
    xEst = MuXpred ;
    PEst = PXpred;
end
end

