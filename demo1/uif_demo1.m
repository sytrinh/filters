clear;
clc;
echo off;
path('../uif',path);

sigmav = 0.1;
sigmaw = 0.5;
Q = sigmav*sigmav;
R = sigmaw*sigmaw;
N = 1000;
x(:,1) = [0;0.05];
xtrue = x;
z = zeros(1,N-1);
rng(1);

for i=2:N
    x(:,i) = [sin(x(2,i-1)*(i-1)) + randn*sigmav; x(2,i-1)];
    z(i) = x(1,i) + randn*sigmaw;
    xtrue(:,i) = [sin(x(2,i-1)*(i-1)); x(2,i-1)];
end

plot(x(1,:), '-b');
hold on;
plot(z,'.r')
hold on;
% plot(xtrue(1,:), '-r')

%%
% Unscented Information Filter
%

x0 = [0;0.04];
P0 = [0.01 0; 0 0.01];
xpred = x0;

for i=2:N
    u = i;
    f = 'fun_f';
    h = 'fun_h';
    if mod(i,1) == 0
        zobs = z(i);
    else
        zobs = [];
    end
    [xEst, PEst] = uif(x0, P0, f, u, Q, h, zobs, R);
    xpred(:,i) = xEst;
    x0 = xEst;
    P0 = PEst;
end

plot(xpred(1,:),'-g')
