clear;
clc;
echo off;
path('../enkf',path);


rng(1);
g = 10;
t = 0:0.05:5;
x(:,1) = [100;0];
z = zeros(1, length(t)-1);
for i=2:length(t)
    dt = t(i) - t(i-1);
    x(:,i) = [1 dt;0 1]*x(:,i-1) + [-1/2*dt^2; -dt]*g;
    temp = x(:,i);
    z(i-1) = temp(1) + 6*randn;
end

plot(t,x(1,:))
hold on;
plot(t(2:end), z, '.r')

%%
% Unscented Kalman Filter
%

x0 = [100;0];
P0 = [10 0; 0 0.01];
Q = [10 0;0 0];
R = 4;

xpred = x0;

for i=2:length(t)
    u = t(i) - t(i-1);
    f = 'fun_f';
    h = 'fun_h';
    if mod(i,5) == 0
        zobs = z(i-1);
    else
        zobs = [];
    end

    [xEst, PEst] = enkf(x0, P0, f, u, Q, h, zobs, R);
    x0 = xEst;
    P0 = PEst;
    xpred(:,i) = x0;
end

plot(t,xpred(1,:),'.g')

