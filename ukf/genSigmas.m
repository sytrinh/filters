function [X, Wm, Wc, L] = genSigmas(mu, P, alpha, beta, kappa)
% Generate sigma points
% Input:
%       mu: mean
%       P: covariance
%       alpha, beta, kappa: Sigma parameters. Alpha should be small
% Output:
%       X: sigma points
%       Wm: weights to calculate means
%       Wc: weights to calculate covariance
%       L: number of sigma points

n = numel(mu);
L = 2*n+1;
lambda = alpha^2*(n+kappa)-n;

A = (chol((n+kappa)*P))';

X = [zeros(n, 1) -A A];
X = X + repmat(mu, 1, L);
Wm = [lambda 0.5*ones(1, L-1)]/(n+lambda);
Wc = [lambda 0.5*ones(1, L-1)]/(n+lambda);
Wc(1) = Wc(1) + (1-alpha^2+beta);
end

