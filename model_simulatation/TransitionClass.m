classdef TransitionClass
    %TRANSITIONCLASS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        vFree = 0;
        kCrit = 0;
        kJam = 0;
        nCell = 0;
        onRampPos = 0; % position of the on-ramp
        offRampPos = 0; % position of the off-ramp
    end
    
    methods
        function obj = TransitionClass(nCell, onRampPos, offRampPos, vFree, kCrit, kJam)
            obj.nCell = nCell;
            obj.onRampPos = onRampPos;
            obj.offRampPos = offRampPos;
            obj.vFree = vFree;
            obj.kCrit = kCrit;
            obj.kJam = kJam;
        end
        
        function q = densityToFlow(obj,ki)
            if ki < obj.kCrit
                q = obj.vFree*ki*(1-ki/obj.kJam);
            else
                q = obj.vFree*obj.kCrit*ki*(1/ki - 1/obj.kJam);
            end
        end

        function S = supplyFun(obj, ki)
            ki = max(ki, obj.kCrit);
            S = obj.densityToFlow(ki);
        end

        function D = demandFun(obj, ki)
            ki = min(ki, obj.kCrit);
            D = obj.densityToFlow(ki);
        end

        function q = qInFun(obj, ki, kMer)
        % ki: density at the current cell
        % kMer: the densities of all the cells that merge into cell i
        %       the top element of kMer is always set to be the internal cell (if there is)
            DVec = zeros(length(kMer),1);
            for i=1:length(kMer)
                DVec(i) = obj.demandFun(kMer(i));
            end
            D = sum(DVec);
            S = obj.supplyFun(ki);
            q = min(S, D);
        end

        function q = qOutFun(obj,ki, kDiv)
        % ki: density at the current cell
        % kDiv: the densities of all the cells that diverse from cell i
        %       the top element of kDiv is always set to be the internal cell (if there is)
            D = obj.demandFun(ki);
            SVec = zeros(length(kDiv), 1);
            for i=1:length(kDiv)
                SVec(i) = obj.supplyFun(kDiv(i));
            end
            S = sum(SVec);
            q = min(S, D);
        end

        function k = transitionFun(obj, kIn, kEx, dt, dsVec)
            k = zeros(length(kIn),1);
            for i=1:length(kIn)
                if i==1
                    kMer = kEx(1);
                    kDiv = kIn(2);
                elseif i==obj.onRampPos
                    kMer = [kIn(i-1); kEx(2)];
                    kDiv = kIn(i+1);
                elseif i==obj.offRampPos
                    kMer = kIn(i-1);
                    kDiv = [kIn(i+1); kEx(3)];
                elseif i==length(kIn)
                    kMer = kIn(i-1);
                    kDiv = kEx(4);
                end
                if i ~= obj.offRampPos+1
                    qIn = obj.qInFun(kIn(i), kMer);
                    qOut = obj.qOutFun(kIn(i), kDiv);
                else
                    qIn = max(obj.densityToFlow(kIn(i-1)) - obj.densityToFlow(kEx(3)),0);
                    qOut = obj.qOutFun(kIn(i), kDiv);
                end
                k(i) = kIn(i) + dt/dsVec(i)*(qIn-qOut);

            end
        end
    end
    
end

