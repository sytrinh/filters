clear; clc;

vFree = 25;
kCrit = 15;
kJam = 25;
nCell = 50;
dt = 5;
nStep = 400; % number of time steps
dsVec = 100*ones(nCell, 1); % length of each cell
kIn0 = 3*ones(nCell, 1); % veh/m
kEx0 = [5; 20; 2; 20]; % veh/m
kIn = kIn0;
kEx = kEx0;
onRampPos = 10; % position of the on-ramp
offRampPos = 40; % position of the off-ramp

t = [0 dt*(1:nStep)];
kStore = zeros(nCell, nStep+1); % store densities in each time step
kStore(:,1) = kIn0;

for i=1:nStep
    % Assuming the densities of external cells do not change in each step
    instance = TransitionClass(nCell, onRampPos, offRampPos, vFree, kCrit, kJam);
    kIn = instance.transitionFun(kIn, kEx, dt, dsVec);
    kStore(:,i+1) = kIn;
end

subplot(2,2,1)
plot(t, kStore(onRampPos-1,:))
title('Before merging cell')
subplot(2,2,2)
plot(t, kStore(onRampPos+1,:))
title('After merging cell')
subplot(2,2,3)
plot(t, kStore(offRampPos-1,:))
title('Before diversing cell')
subplot(2,2,4)
plot(t, kStore(offRampPos+1,:))
title('After diversing cell')